module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

/*
        concat: {
            dist: {
                src: [
                    'style/js/alert.js', // Все JS в папке libs
                    'style/js/tools.js'  // Конкретный файл
                ],
                dest: 'js/build/production.js'
            }   
        },

        uglify: {
            options: {
                mangle: true
            }, // <-------
            build: {
                src: 'js/build/production.js',
                dest: 'js/build/production.min.js'
            }
        },
*/

        less: {
            development: {
            // options: {
            //   compress: true,
            //   yuicompress: true,
            //   optimization: 2
            // },
            files: {
              // target.css file: source.less file
              "style/css/structure.css": "style/less/imports.less"
            }
          }
        },

        watch: {
            scripts: {
                files: ['style/less/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false,
                },
            }
        }

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    /*
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    */
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['less', 'watch']);

};