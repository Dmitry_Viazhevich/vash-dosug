$(document).ready(function() {
	$.fn.swipe.defaults.excludedElements = $.fn.swipe.defaults.excludedElements.replace(' a,','');
	var $masContainer = $('.masonry-grid');
	// initialize
	$masContainer.masonry({
		itemSelector: '.masonry-col',
		isFitWidth: true,
		columnWidth: 211
	});

	$('.top-search__select').customSelect();
	$('.js-transparent-select').customSelect();
	$('.js-black-select').customSelect();

	$.slidebars({
		siteClose: true,
		scrollLock: false,
	});

	
    function accordeon (action) {
		$('.js-widget-nav').map(function() {
	        var nav = $(this),
	            button = nav.find('.js-title'),
	            menu = nav.find('.js-content');

	        button.off('click');
			if (action == 'on') {
		        button.on('click', function(e) {
		            e.preventDefault();
		            menu.slideToggle();
		            button.toggleClass('toggled-on');
		            menu.toggleClass('toggled-on');
		        });
			}

	    });
	}
	
	$('.rating-leisure-tabs__nav-i a').on({'touchstart' : function(e){
		e.preventDefault();
		$('.rating-leisure-tab--show').removeClass('rating-leisure-tab--show');
		$($(this).attr('href')).addClass('rating-leisure-tab--show');

		$('.rating-leisure-tabs__nav-i--current').removeClass('rating-leisure-tabs__nav-i--current');
		$(this).parent().addClass('rating-leisure-tabs__nav-i--current');
	}});

	// Carousel on the front page
	function carouFredSelInit (Slider, visible, responsive, width, height, autoplay, align) {
		if (typeof $().carouFredSel == 'function') {

			// Cache slide navigation
			Slider.each(function(i, iSlider) {
				
				iSlider = $(iSlider);

				var sliderContainer = iSlider.parents('.js-slider-container'),

					options = { // Common slider options
						// width: '100%',
						// height: 'auto',				
						width: width,
						height: height,
						items: {
							visible: visible
							// height: '100%'
						},
						align: align,
						responsive: responsive,
						circular: false,
						scroll: {
							items: 1,
							fx: 'directscroll',
							duration: 600,
							// onAfter: function ( e ) {
							// 	if (iSlider.triggerHandler('currentPage') == 0) {
							// 		$(this).animate({left: -222});
							// 	}
							// }
						},
						auto: {
							play: autoplay,
							timeoutDuration: 7000
						},
						prev        : {
					        button      : sliderContainer.find(".js-prev")
					    },
					    next        : {
					        button      : sliderContainer.find(".js-next")
					    },
						swipe: {
						    onTouch: true,
						    onMouse: true
						},
						pagination: {
							container: sliderContainer.find('.js-slider-pager')
						},
					};

				iSlider.carouFredSel(options);
				if (align === 'center'){
					var pagerItems = sliderContainer.find('.js-slider-pager a');
					pagerItems.eq(-1).hide();
					pagerItems.eq(-2).hide();
				}
			});
		}
	}

	$('.js-content').hide();


	resizeHandler();
	


	$(window).on('resize', resizeHandler);
	function resizeHandler () {

		if ($(window).width() < 768) {

			carouFredSelInit($('.js-slider'), 3, false, $(window).width(), null, false, 'center');

			carouFredSelInit($('.js-slider-mobile'), 3, false, $(window).width(), null, false, 'center');

			carouFredSelInit($('.js-slider-mobile-fullwidth'), 1, true, '100%', "auto", false, 'center');

			
			$masContainer.masonry("destroy");

			carouFredSelInit($('.js-slider-mobile-masonry'), 3, false, $(window).width(), null, false, 'center');

			accordeon('on');
		};


		// if ($(window).width() < 643) {
		// 	carouFredSelInit($('.js-slider-mobile-masonry'), 2);
		// 	carouFredSelInit($('.js-slider'), 2);
		// 	carouFredSelInit($('.js-slider-mobile'), 2);
		// };

		// if ($(window).width() < 433) {
		// 	carouFredSelInit($('.js-slider-mobile-masonry'), 1);
		// 	carouFredSelInit($('.js-slider-mobile'), 1);
		// 	carouFredSelInit($('.js-slider'), 1);
		// };

		// if ($(window).width() > 433) {
		// 	carouFredSelInit($('.js-slider-mobile-masonry'), 2);
		// 	carouFredSelInit($('.js-slider-mobile'), 2);
		// 	carouFredSelInit($('.js-slider'), 2);
		// };

		// if ($(window).width() > 643) {
		// 	carouFredSelInit($('.js-slider-mobile-masonry'), 3);
		// 	carouFredSelInit($('.js-slider'), 3);
		// 	carouFredSelInit($('.js-slider-mobile'), 3);
		// };

		if ($(window).width() >= 768) {
			$('.desktop-hide').remove();
			carouFredSelInit($('.js-slider'), 4, false, '100%', null, false, 'left');

			$('.js-content').show();

			$('.js-slider-mobile').trigger("destroy");
			$('.js-slider-mobile-fullwidth').trigger("destroy");
			$('.js-slider-mobile-masonry').trigger("destroy");

			$masContainer.masonry('destroy');
			$masContainer.masonry({
				itemSelector: '.masonry-col',
				isFitWidth: true,
				columnWidth: 211
			});

			accordeon('off');
		};
	}

});
